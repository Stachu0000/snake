package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;

import java.util.ArrayList;

public class ControlMove {
    Snake game;
    SpriteBatch batch;
    OrthographicCamera camera;
    Vector3 touchPos;
    SnakeSection snakeSection;
    Float step;
    Rectangle leftButtonRectangle;
    Rectangle rightButtonRectangle;
    private boolean leftArrowTouched = true;
    private boolean rightArrowTouched = true;

    public ControlMove(Snake game, SnakeSection snakeSection) {
        this.game = game;
        this.batch = game.getBatch();
        this.step = snakeSection.getPosition().getWidth();
        this.camera = game.getCamera();
        this.snakeSection = snakeSection;
        this.touchPos = new Vector3();
        this.leftButtonRectangle = game.getMyActor3().getLeftButtonRectangle();
        this.rightButtonRectangle = game.getMyActor3().getRightButtonRectangle();
    }

    public void moveSnakeHead(SnakeSection snakeHead, MyActor2 myActor2, GameScreen gameScreen, ArrayList<SnakeSection> sections) {
        Food food = myActor2.getFood();

        //If snakeHead is outside playFiled or collides with any other section of the snake calls Game Over Screen
        if (!snakeHeadIsInPlayField(myActor2) || snakeHeadCollidesWithOtherSections(sections, snakeHead)) {
            game.setScreen(new GameOverScreen(game));
            gameScreen.dispose();
        }

        snakeHeadMakeOneStep(snakeHead);
    }

    //Every section (beside snakeHead) checks position of the section before
    public void moveSection(ArrayList<SnakeSection> sections) {
        for (int i = 1; i < sections.size(); i++) {
            //setup current position
            Rectangle currentSectionPosition = sections.get(i).getPosition();
            //checks last position of the section before (index - 1)
            Rectangle preSectionPosition = sections.get(i - 1).getLastPosition();

            //saves current position as the last position
            sections.get(i).setLastPosition();
            //set current position as last position of the section before (index - 1)
            currentSectionPosition.set(preSectionPosition);
        }
    }

    public void changeMove(SnakeSection snakeSection) {

        if (Gdx.input.isTouched()) {
            touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
            camera.unproject(touchPos);

            //the screen renders 60 fps, so one touch of the direction button could change several times
            //one touch sets left and right arrowTouched to false, so holding button longer don't change snake direction
            if (leftButtonRectangle.contains(touchPos.x, touchPos.y) && leftArrowTouched) {
                moveLeft(snakeSection);
                leftArrowTouched = false;
            } else if (rightButtonRectangle.contains(touchPos.x, touchPos.y) && rightArrowTouched) {
                moveRight(snakeSection);
                rightArrowTouched = false;
            }
        } else {
            //reset when screen is not touched
            leftArrowTouched = true;
            rightArrowTouched = true;
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.LEFT)) {
            moveLeft(snakeSection);

        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.RIGHT)) {
            moveRight(snakeSection);

        }
    }

    private boolean snakeHeadIsInPlayField(MyActor2 myActor2) {
        return myActor2.getPlayField().contains(snakeSection.getPosition());
    }

    private boolean snakeHeadCollidesWithOtherSections(ArrayList<SnakeSection> sections, SnakeSection snakeHead) {
        return sections.stream()
                .skip(1)
                .anyMatch(s -> s.getPosition().overlaps(snakeHead.getPosition()));
    }

    //Before snakeHead makes next step it saves last position for next section position
    private void snakeHeadMakeOneStep(SnakeSection snakeSection) {
        Rectangle position = snakeSection.getPosition();

        if (snakeSection.movingEast) {
            //saves snakeHead last position for next snakeSection
            snakeSection.setLastPosition();
            //then change direction of snake movement
            position.x += step;
        } else if (snakeSection.movingWest) {
            snakeSection.setLastPosition();
            position.x -= step;
        } else if (snakeSection.movingNorth) {
            snakeSection.setLastPosition();
            position.y += step;
        } else if (snakeSection.movingSouth) {
            snakeSection.setLastPosition();
            position.y -= step;
        }
    }

    private void moveLeft(SnakeSection snakeSection) {

        if (snakeSection.movingEast) {
            snakeSection.movingEast = false;
            snakeSection.movingNorth = true;
        } else if (snakeSection.movingWest) {
            snakeSection.movingWest = false;
            snakeSection.movingSouth = true;
        } else if (snakeSection.movingNorth) {
            snakeSection.movingNorth = false;
            snakeSection.movingWest = true;
        } else if (snakeSection.movingSouth) {
            snakeSection.movingSouth = false;
            snakeSection.movingEast = true;
        }
    }

    private void moveRight(SnakeSection snakeSection) {
        if (snakeSection.movingEast) {
            snakeSection.movingEast = false;
            snakeSection.movingSouth = true;
        } else if (snakeSection.movingWest) {
            snakeSection.movingWest = false;
            snakeSection.movingNorth = true;
        } else if (snakeSection.movingNorth) {
            snakeSection.movingNorth = false;
            snakeSection.movingEast = true;
        } else if (snakeSection.movingSouth) {
            snakeSection.movingSouth = false;
            snakeSection.movingWest = true;
        }
    }
}
