package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class MyActor3 extends Actor {
    Snake game;
    Texture leftButtonTexture;
    Texture rightButtonTexture;
    Rectangle leftButtonRectangle;
    Rectangle rightButtonRectangle;

    public MyActor3(Snake game) {
        this.game = game;
        this.leftButtonTexture = game.getLeftButtonTexture();
        this.rightButtonTexture = game.getRightButtonTexture();
        this.leftButtonRectangle = new Rectangle(15, 10, 64, 64);
        this.rightButtonRectangle = new Rectangle(game.getWidth() - 79, 10, 64, 64);

    }

    @Override
    public void draw(Batch batch, float parentAlpha){
        batch.draw(leftButtonTexture, leftButtonRectangle.x, leftButtonRectangle.y, leftButtonRectangle.getWidth(), leftButtonRectangle.getHeight());
        batch.draw(rightButtonTexture, rightButtonRectangle.x, rightButtonRectangle.y, rightButtonRectangle.getWidth(), rightButtonRectangle.getHeight());
    }

    public Rectangle getLeftButtonRectangle() {
        return leftButtonRectangle;
    }

    public Rectangle getRightButtonRectangle() {
        return rightButtonRectangle;
    }
}
