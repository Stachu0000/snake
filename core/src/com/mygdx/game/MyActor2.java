package com.mygdx.game;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;

import java.util.ArrayList;
import java.util.Random;

public class MyActor2 extends Actor {
    private Snake game;
    private MyActor1 myActor1;
    private SnakeSection snakeHead;
    private Rectangle playField;
    private Texture wallpaper;
    private Food food;
    private Random random = new Random();
    private ArrayList<SnakeSection> snakeSections = new ArrayList<>();

    public MyActor2(Snake game, OrthographicCamera camera) {
        this.game = game;
        this.myActor1 = game.getMyActor1();
        this.snakeHead = new SnakeSection(game, 15, 100);
        snakeHead.setTexture(game.getSnakeHeadTexture());
        this.playField = new Rectangle(14, 99, game.getWidth() - 28, 512);
        this.wallpaper = game.getPlayFieldWallpaper();
        makeNewFood();
        snakeSections.add(snakeHead);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {

        Texture snakeTexture = snakeHead.getTexture();
        Rectangle headPosition = snakeHead.getPosition();

        //x - 3, y - 3, width + 6, height + 6 - wallpaper has a frame which is 3 pixels wide and is outside playField
        batch.draw(wallpaper, playField.x - 3, playField.y -3, playField.getWidth() + 6, playField.getHeight() + 6);
        batch.draw(snakeTexture, headPosition.x, headPosition.y, headPosition.getWidth(), headPosition.getHeight());
        snakeSections.forEach(s -> batch.draw(s.getTexture(), s.getPosition().x, s.getPosition().y, s.getPosition().getWidth(), s.getPosition().getHeight()));
        drawFood(batch);
    }

    private void drawFood(Batch batch) {

        Texture foodTexture = food.getFoodTexture();
        Rectangle foodPosition = food.getFoodPosition();

        batch.draw(foodTexture, foodPosition.x, foodPosition.y, foodPosition.getWidth(), foodPosition.getHeight());
    }

    public void developSnakeWhenEatsFood(GameScreen screen) {
        if (snakeEatsFood()) {
            makeNewFood();
            addNewSnakeSection();

            myActor1.addOnePoint();
            changeMoveInterval(screen);
        }
    }

    //acceleration depends on points quantity and it's decreasing
    private void changeMoveInterval(GameScreen screen) {
        if (myActor1.getPoints() < 10) {
            screen.setMoveInterval(0.02f);
        } else if (myActor1.getPoints() < 15) {
            screen.setMoveInterval(0.01f);
        } else {
            screen.setMoveInterval(0.005f);
        }
    }

    private void addNewSnakeSection() {
        //checks last position of the last in ArrayList<SnakeSection> snake section
        float lastX = snakeSections.get(snakeSections.size() - 1).getLastPosition().x;
        float lastY = snakeSections.get(snakeSections.size() - 1).getLastPosition().y;

        snakeSections.add(new SnakeSection(game, lastX, lastY));
    }

    public ArrayList<SnakeSection> getSnakeSections() {
        return snakeSections;
    }

    private boolean snakeEatsFood() {
        return snakeHead.getPosition().overlaps(food.getFoodPosition());
    }

    private void makeNewFood() {
        this.food = new Food(game, this, random.nextInt(game.getFoodTextures().size()));
    }

    public Food getFood() {
        return food;
    }

    public SnakeSection getSnakeHead() {
        return snakeHead;
    }

    public Rectangle getPlayField() {
        return playField;
    }


}
