package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;

import java.util.ArrayList;
import java.util.Random;

public class Food {
    private MyActor2 myActor2;
    private Texture foodTexture;
    private Rectangle foodPosition;
    private ArrayList<SnakeSection> sections;

    public Food(Snake game, MyActor2 myActor2, int foodIndex) {
        this.foodTexture = game.getOneFoodTexture(foodIndex);
        this.myActor2 = myActor2;
        this.sections = myActor2.getSnakeSections();
        setRandomFoodPosition();
    }

    private void setRandomFoodPosition() {
        Random random = new Random();

        float foodX = random.nextInt((int) (myActor2.getPlayField().getWidth() / 30));
        float foodY = random.nextInt((int) (myActor2.getPlayField().getHeight() / 30));

        foodPosition =  new Rectangle(15 + (foodX * 30), 100 + (foodY * 30), 30, 30);

        //add new method on stack as long as random food could appear inside snake body
        if(randomFoodIsInsideSnakeBody()){
            setRandomFoodPosition();
        }
    }

    private boolean randomFoodIsInsideSnakeBody() {
        return sections.stream().anyMatch(s -> s.getPosition().overlaps(foodPosition));
    }

    public Texture getFoodTexture() {
        return foodTexture;
    }

    public Rectangle getFoodPosition() {
        return foodPosition;
    }
}
