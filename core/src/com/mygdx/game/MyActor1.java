package com.mygdx.game;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class MyActor1 extends Actor {
    private Snake game;
    private BitmapFont font;
    private int points = 0;
    public MyActor1(Snake game) {
        this.game = game;
        this.font = game.getFont();
    }

    @Override
    public void draw(Batch batch, float parentAlpha){
        font.setColor(Color.BLACK);
        font.getData().setScale(1.0f);
        font.draw(batch, "Score : " + points, 15, game.getHeight() * 0.97f);
    }

    public void addOnePoint(){
        points += 1;
    }

    public int getPoints() {
        return points;
    }
}
