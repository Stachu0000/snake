package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.ScreenUtils;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import java.util.ArrayList;


public class GameScreen implements Screen {
    private final Snake game;
    private SpriteBatch batch;
    private OrthographicCamera camera;
    private Viewport viewport;
    private Stage stage;
    private float timeSinceLastMove;
    private float moveInterval = 0.5f;
    private SnakeSection snakeHead;
    private ControlMove controlMove;
    private MyActor2 myActor2;
    private ArrayList<SnakeSection> sections;
    private Texture mainScreenWallpaper;

    public GameScreen(Snake game, Stage stage) {
        this.game = game;
        this.batch = game.getBatch();
        this.stage = stage;
        this.myActor2 = game.getMyActor2();
        this.snakeHead = myActor2.getSnakeHead();
        this.controlMove = new ControlMove(game, snakeHead);
        this.camera = game.getCamera();
        this.viewport = new FitViewport(game.getWidth(), game.getHeight(), camera);
        this.sections = game.getMyActor2().getSnakeSections();
        this.mainScreenWallpaper = game.getMainScreenWallpaper();
        camera.position.set(game.getWidth() / 2f, game.getHeight() / 2f, 0);
    }

    @Override
    public void render(float delta) {
        timeSinceLastMove += delta;

        ScreenUtils.clear(0.75f, 0.75f, 0.75f, 0.5f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();
        batch.setProjectionMatrix((camera.combined));

        controlMove.changeMove(snakeHead);

        //display movement only on time interval not in every screen render
        if (timeSinceLastMove >= moveInterval) {
            //moving snake head and change its directions
            controlMove.moveSnakeHead(snakeHead, myActor2, this, sections);

            //moving every section beside snake head
            controlMove.moveSection(sections);

            //add new section after food is eaten
            myActor2.developSnakeWhenEatsFood(this);


            timeSinceLastMove -= moveInterval;
        }

        batch.begin();
        batch.draw(mainScreenWallpaper,0,0);
        batch.end();

        stage.act(delta);
        stage.draw();

    }

    public void setMoveInterval(float moveInterval) {
        this.moveInterval -= moveInterval;
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height, true);
        camera.position.set(game.getWidth() / 2f, game.getHeight() / 2f, 0);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void show() {

    }

    @Override
    public void dispose() {

    }

}
