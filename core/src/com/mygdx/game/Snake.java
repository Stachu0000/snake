package com.mygdx.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import java.util.ArrayList;

public class Snake extends Game {
    private SpriteBatch batch;
    private BitmapFont font;
    private final float width = 360;
    private final float height = 670;
    private Stage stage;
    private MyActor1 myActor1;
    private MyActor2 myActor2;
    private MyActor3 myActor3;
    private Viewport viewport;
    private OrthographicCamera camera;
    private Texture leftButtonTexture;
    private Texture rightButtonTexture;
    private Texture snakeHeadTexture;
    private Texture snakeSectionTexture;
    private Texture playFieldWallpaper;
    private Texture gameScreenWallpaper;
    private Texture mainMenuWallpaper;
    private ArrayList<Texture> foods = new ArrayList<>();
    private Texture gameOverTexture;//

    @Override
    public void create() {
        batch = new SpriteBatch();
        font = new BitmapFont();
        camera = new OrthographicCamera();
        viewport = new FitViewport(width, height, camera);


        this.leftButtonTexture = new Texture(Gdx.files.internal("button.png"));
        this.rightButtonTexture = new Texture(Gdx.files.internal("button.png"));
        this.snakeHeadTexture = new Texture(Gdx.files.internal("snakeHead.png"));
        this.snakeSectionTexture = new Texture(Gdx.files.internal("snakeSection.png"));
        this.playFieldWallpaper = new Texture(Gdx.files.internal("wallpaper.png"));
        this.foods.add(new Texture(Gdx.files.internal("food1.png")));
        this.foods.add(new Texture(Gdx.files.internal("food2.png")));
        this.foods.add(new Texture(Gdx.files.internal("food3.png")));
        this.foods.add(new Texture(Gdx.files.internal("food4.png")));
        this.foods.add(new Texture(Gdx.files.internal("food5.png")));
        this.foods.add(new Texture(Gdx.files.internal("food6.png")));
        this.gameOverTexture = new Texture(Gdx.files.internal("gameOverWallpaper.png"));
        this.gameScreenWallpaper = new Texture(Gdx.files.internal("mainWallpaper.png"));
        this.mainMenuWallpaper = new Texture(Gdx.files.internal("mainMenuWallpaper.png"));

        setStageAndActors();
        this.setScreen(new MainMenuScreen(this, stage));
    }

    //Divide main game screen for 3 sections, so every section can display objects independent
    private void setStageAndActors() {
        stage = new Stage(viewport, batch);
        Gdx.input.setInputProcessor(stage);

        myActor1 = new MyActor1(this);
        myActor2 = new MyActor2(this, camera);
        myActor3 = new MyActor3(this);

        myActor1.setBounds(0, 610, width, 60);
        myActor2.setBounds(0, 100, width, 510);
        myActor3.setBounds(0, 0, width, 99);

        stage.addActor(myActor1);
        stage.addActor(myActor2);
        stage.addActor(myActor3);

    }

    public SpriteBatch getBatch() {
        return batch;
    }

    public BitmapFont getFont() {
        return font;
    }

    public OrthographicCamera getCamera() {
        return camera;
    }

    public Texture getGameOverTexture() {
        return gameOverTexture;
    }

    public Texture getSnakeHeadTexture() {
        return snakeHeadTexture;
    }

    public Texture getSnakeSectionTexture() {
        return snakeSectionTexture;
    }

    public ArrayList<Texture> getFoodTextures() {
        return foods;
    }

    public Texture getOneFoodTexture(int index) {
        return foods.get(index);
    }

    public Texture getPlayFieldWallpaper() {
        return playFieldWallpaper;
    }

    public MyActor1 getMyActor1() {
        return myActor1;
    }

    public MyActor2 getMyActor2() {
        return myActor2;
    }

    public MyActor3 getMyActor3() {
        return myActor3;
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

    public Texture getLeftButtonTexture() {
        return leftButtonTexture;
    }

    public Texture getRightButtonTexture() {
        return rightButtonTexture;
    }

    public Texture getMainScreenWallpaper() {
        return gameScreenWallpaper;
    }

    public Texture getMainMenuWallpaper() {
        return mainMenuWallpaper;
    }

    @Override
    public void render() {
        super.render();
    }

    @Override
    public void dispose() {
        batch.dispose();
        font.dispose();
        leftButtonTexture.dispose();
        rightButtonTexture.dispose();
        snakeHeadTexture.dispose();
        playFieldWallpaper.dispose();
        foods.forEach(Texture::dispose);
        gameOverTexture.dispose();
        gameScreenWallpaper.dispose();
        mainMenuWallpaper.dispose();
        snakeSectionTexture.dispose();
    }
}
