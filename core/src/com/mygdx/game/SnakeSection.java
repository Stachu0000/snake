package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;

public class SnakeSection {
    private Rectangle position;
    private Rectangle lastPosition = new Rectangle();
    private Texture texture;
    boolean movingEast = true;
    boolean movingWest = false;
    boolean movingNorth = false;
    boolean movingSouth = false;


    public SnakeSection(Snake game, float startingPointX, float startingPointY) {
        this.position = new Rectangle(startingPointX, startingPointY, 30, 30);
        this.texture = game.getSnakeSectionTexture();
    }


    public Rectangle getPosition() {
        return position;
    }

    public Rectangle getLastPosition() {
        return lastPosition;
    }

    public void setLastPosition(){
        lastPosition.set(position);
    }


    public Texture getTexture() {
        return texture;
    }

    public void setTexture(Texture texture) {
        this.texture = texture;
    }
}
