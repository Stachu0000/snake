package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.ScreenUtils;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

public class GameOverScreen implements Screen {


    final Snake game;
    SpriteBatch batch;
    BitmapFont font;
    OrthographicCamera camera;
    Viewport viewport;
    Texture gameOverTexture;
    Rectangle gameOverRectangle;
    int points;

    public GameOverScreen(Snake game) {
        this.game = game;
        this.batch = game.getBatch();
        this.font = game.getFont();
        this.camera = new OrthographicCamera();
        this.viewport = new FitViewport(game.getWidth(), game.getHeight(), camera);
        this.gameOverTexture = game.getGameOverTexture();
        this.gameOverRectangle = new Rectangle(0,0,game.getWidth(), game.getHeight());
        this.points = game.getMyActor1().getPoints();
        camera.position.set(game.getWidth() / 2f, game.getHeight() / 2f, 0);
    }


    @Override
    public void render(float delta) {
        ScreenUtils.clear(0, 0, 0, 1);

        camera.update();
        batch.setProjectionMatrix((camera.combined));

        batch.begin();
        font.setColor(Color.BLACK);
        font.getData().setScale(2.0f);

        batch.draw(gameOverTexture, gameOverRectangle.x, gameOverRectangle.y, gameOverRectangle.getWidth(), gameOverRectangle.getHeight());
        font.draw(batch, points + " !!!", 230, 160 );

        batch.end();

    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
        camera.position.set(game.getWidth() / 2f, game.getHeight() / 2f, 0);
    }

    @Override
    public void show() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
